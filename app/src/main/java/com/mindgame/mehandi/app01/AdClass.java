package com.mindgame.mehandi.app01;


import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.util.Log;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.mindgame.mehandi.app01.ads.AdMobdetails_singleton;

public class AdClass {

	Context c;
	LinearLayout layout, layout1;
	// AdMOb
	AdView adView;
	String AD_UNIT_ID = "";


	AdView adView1;
	String AD_UNIT_ID1 = "";

	InterstitialAd interstitial;
	String AD_UNIT_ID_full_page = "";

	InterstitialAd interstitial1;
	String AD_UNIT_ID_full_page1 = "";
	
	
	//for banner: ca-app-pub-3940256099942544/6300978111
	//for interstitial: ca-app-pub-3940256099942544/1033173712



	/*
	 * // RevMob RevMob revmob; RevMobBanner banner; // StartApp private
	 * StartAppAd startAppAd;
	 * 
	 * // MM Ads
	 * 
	 * MM mm; MMBestApps mmb
	 */;

	public LinearLayout layout_strip(Context context) {

		c = context;
		Log.d("TASG", "entered layout code");

		layout = new LinearLayout(c);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

		layout.setLayoutParams(params);
		layout.setOrientation(LinearLayout.HORIZONTAL);

		return layout;
	}

	public void AdMobBanner(Context c, String BANNER_UNIT_ID) {

		adView = new AdView(c);
		adView.setAdSize(AdSize.SMART_BANNER);
        BANNER_UNIT_ID = BANNER_UNIT_ID.trim();
		adView.setAdUnitId(BANNER_UNIT_ID);

		layout.addView(adView);

		AdRequest adRequest = new AdRequest.Builder().build();
		adView.loadAd(adRequest);

	}

	public void AdMobInterstitial(Context c) {
		interstitial = new InterstitialAd(c);
		interstitial.setAdUnitId(AD_UNIT_ID_full_page);

		// Create ad request.
		AdRequest adRequest = new AdRequest.Builder().build();
		// Begin loading your interstitial.
		interstitial.loadAd(adRequest);
		interstitial.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				// TODO Auto-generated method stub
				super.onAdLoaded();
				interstitial.show();
			}
		});

	}

	public void AdMobInterstitial1(Context c) {
		interstitial1 = new InterstitialAd(c);
		interstitial1.setAdUnitId(AD_UNIT_ID_full_page1);

		// Create ad request.
		AdRequest adRequest = new AdRequest.Builder().build();
		// Begin loading your interstitial.
		interstitial1.loadAd(adRequest);
		interstitial1.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				// TODO Auto-generated method stub
				super.onAdLoaded();
				interstitial1.show();
			}
		});

	}

	/*
	 * // RevMob // RevMob
	 * 
	 * public void RevMobBanner(Activity activity) {
	 * 
	 * revmob = RevMob.start(activity); banner = revmob.createBanner(activity);
	 * // layout.addView(banner); revmob.showBanner(activity);
	 * 
	 * }
	 * 
	 * public void RevMobFullPageAd(Activity activity) {
	 * 
	 * revmob = RevMob.start(activity); revmob.showFullscreen(activity);
	 * 
	 * }
	 * 
	 * 
	 * public void StartAppAds(Activity activity) { startAppAd = new
	 * StartAppAd(activity); StartAppSDK.init(activity, "110437331",
	 * "203250348", true); startAppAd.showAd(); startAppAd.loadAd();
	 * startAppAd.onBackPressed(); startAppAd.onResume(); startAppAd.onPause();
	 * }
	 * 
	 * public void StartAppFullPage(Activity activity) { startAppAd = new
	 * StartAppAd(activity); StartAppSDK.init(activity, "110437331",
	 * "203250348", true); startAppAd.showAd(); startAppAd.loadAd(); }
	 * 
	 * public void StartAppOnBack(Activity activity) {
	 * 
	 * startAppAd.onBackPressed(); }
	 * 
	 * // MM Ads
	 * 
	 * public void MMAds(Activity activity) {
	 * 
	 * mmb = new MMBestApps(activity); mmb.ShowDynamicAppWall(activity); mm =
	 * new MM(activity);
	 * 
	 * }
	 * 
	 * public void MMAppWall(Activity activity) {
	 * 
	 * mm = new MM(activity); mm.ShowAppWall(activity);
	 * 
	 * }
	 */

}
