package com.mindgame.mehandi.app01;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.mindgame.mehandi.app01.adapter.DataAdapter;
import com.mindgame.mehandi.app01.ads.AdMobdetails_singleton;

import java.util.ArrayList;

public class Activity_SetOne extends Activity {
    // Remote Config keys
//    private static final String LOADING_PHRASE_CONFIG_KEY = "loading_phrase";
    private static final String WELCOME_MESSAGE_KEY = "welcome_message";
//    private static final String WELCOME_MESSAGE_CAPS_KEY = "welcome_message_caps";

    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    //TextView decleration
    TextView textView;
    InterstitialAd interstitialAd;

    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();
    TextView Title1;
    ProgressDialog pd;



    public ArrayList<String> images_json;
    //URLs of the images to load in the gallery1



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //assigning the layout
        setContentView(R.layout.activity_main);
        /*Get the JSON data from firebase database*/


        //AdMob();
        images_json = new ArrayList<String>();
        Title1 = (TextView) findViewById(R.id.heading);
        Title1.setText(singleton_images.getInstance().getApp_name());
        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        String Banner_Id = AdMobdetails_singleton.getInstance().get_admob_id(0,"banner");
        ad.AdMobBanner(this, Banner_Id);
        images_json = singleton_images.getInstance().get_set_images(0);

        initViews();



    }


    private void initViews() {
        //registering the textview
        textView=(TextView)findViewById(R.id.heading);
    //recycler view registration
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        //when you are adding or removing items in the RecyclerView and that doesn't change it's height or the width.
        recyclerView.setHasFixedSize(true);
        //assigning gridlayout to the recyclerview in two columns
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(),2);
        recyclerView.setLayoutManager(layoutManager);

      //Assigning the ulrs to the arraylist
        //ArrayList image_Urls = prepareData();
        //sending the array list to the dataadapter class
        DataAdapter adapter = new DataAdapter(getApplicationContext(), images_json);
        //set the adapter to the recycler view
        recyclerView.setAdapter(adapter);


    }

    public void AdMob(){
        AdRequest adRequest = new AdRequest.Builder().build();
        interstitialAd=new InterstitialAd(this);
        interstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                Log.d("AD-EVENT","onAdClosed.");
                Toast.makeText(Activity_SetOne.this, "onAdClosed", Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                Log.d("AD-EVENT","onAdFailedToLoad.");
                Toast.makeText(Activity_SetOne.this, "onAdFailedToLoad", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
                Log.d("AD-EVENT","onAdLeftApplication.");
                Toast.makeText(Activity_SetOne.this, "onAdLeftApplication", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                Log.d("AD-EVENT","onAdOpened");
                Toast.makeText(Activity_SetOne.this, "onAdOpened", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                interstitialAd.show();
                Log.d("AD-EVENT","onAdLoaded.");
                Toast.makeText(Activity_SetOne.this, "onAdLoaded", Toast.LENGTH_SHORT).show();
            }
        });
        interstitialAd.loadAd(adRequest);
    }

}
