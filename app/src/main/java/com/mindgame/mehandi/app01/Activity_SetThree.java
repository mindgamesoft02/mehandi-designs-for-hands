package com.mindgame.mehandi.app01;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.mindgame.mehandi.app01.adapter.DataAdapter;
import com.mindgame.mehandi.app01.ads.AdMobdetails_singleton;

import java.util.ArrayList;

public class Activity_SetThree extends Activity {
    // Remote Config keys
//    private static final String LOADING_PHRASE_CONFIG_KEY = "loading_phrase";
    private static final String WELCOME_MESSAGE_KEY = "welcome_message";
//    private static final String WELCOME_MESSAGE_CAPS_KEY = "welcome_message_caps";

    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    //TextView decleration
    TextView textView;
    InterstitialAd interstitialAd;
    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();
    TextView Title3;


    public ArrayList<String> images_json;
    //URLs of the images to load in the gallery1


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //assigning the layout
        setContentView(R.layout.activity_main);
        /*Get the JSON data from firebase database*/
        //AdMob();
        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        String Banner_Id = AdMobdetails_singleton.getInstance().get_admob_id(0,"banner");
        ad.AdMobBanner(this,Banner_Id);
        images_json = new ArrayList();
        images_json = singleton_images.getInstance().get_set_images(2);
        Title3 = (TextView) findViewById(R.id.heading);
        Title3.setText(singleton_images.getInstance().getApp_name());

        initViews();

    }


    private void initViews() {
        //registering the textview
        textView = (TextView) findViewById(R.id.heading);
        //recycler view registration
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        //when you are adding or removing items in the RecyclerView and that doesn't change it's height or the width.
        recyclerView.setHasFixedSize(true);
        //assigning gridlayout to the recyclerview in two columns
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerView.setLayoutManager(layoutManager);

        //Assigning the ulrs to the arraylist
        //ArrayList image_Urls = prepareData();
        //sending the array list to the dataadapter class
        DataAdapter adapter = new DataAdapter(getApplicationContext(), images_json);
        //set the adapter to the recycler view
        recyclerView.setAdapter(adapter);

    }
}
