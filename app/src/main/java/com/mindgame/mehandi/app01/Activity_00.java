package com.mindgame.mehandi.app01;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mindgame.mehandi.app01.ads.AdMobdetails_singleton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Activity_00 extends Activity {
	@Override
	protected void onResume() {
		super.onResume();
		app_name.setText(singleton_images.getInstance().getApp_name());
	}

	LinearLayout layout, strip, layout1, strip1;
	AdMobdetails_singleton ad = AdMobdetails_singleton.getInstance() ;
	ProgressBar spinner;
	Boolean network;
	Context c;
	PopupWindow pop;
	InterstitialAd interstitialAd;
	String interstitial;
	AdMobdetails_singleton AdMob=AdMobdetails_singleton.getInstance();
	String INTERSTITIAL_KEY="interstitial";
	TextView app_name, terms_privacy;;
    ProgressDialog pd;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_00);
		c=getApplicationContext();
        pd =  new ProgressDialog(this);
		app_name = (TextView) findViewById(R.id.app_name);

		layout = (LinearLayout) findViewById(R.id.admob);



		network=AppStatus.getInstance(c).isOnline();
		Log.d("Network Checking :",network.toString());
		networkChecking();

        /*Show spinner*/

        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setIndeterminate(true);
        pd.setMax(50);
        pd.setMessage("loading .....");
        pd.show();




        getJSONdata(); // old Volley reading logic
        /*Reading from Firebase DB directly*/
//        getJSONFromFirebase("mehandi_designs_for_hands");

		terms_privacy=(TextView)findViewById(R.id.privacy);
		terms_privacy.setPaintFlags(terms_privacy.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

		terms_privacy.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Activity_00.this, PrivacyPolicy.class);
				startActivity(i);
			}
		});

	}


	public void gallery(View v) {

		Intent i = new Intent(Activity_00.this, Activity_01.class);
		i.putExtra("ads",true
		);

		startActivity(i);

	}

	public void mywork(View v) {
		//ad.AdMobInterstitial(SecondActivity.this);
		Intent i = new Intent(
				Intent.ACTION_VIEW,
				Uri.parse("https://play.google.com/store/apps/developer?id=Andromida%20apps&hl=en"));

		startActivity(i);

	}
	public void favourite(View v) {
		//ad.AdMobInterstitial1(SecondActivity.this);
		Intent i = new Intent(Activity_00.this, MyImages.class);

		startActivity(i);


	}


	public void rate(View v) {
		//ad.AdMobInterstitial1(SecondActivity.this);
		
		Intent i = new Intent(
				Intent.ACTION_VIEW,
				Uri.parse("https://play.google.com/store/apps/details?id="+getApplicationContext().getPackageName()));

		startActivity(i);
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.addCategory(Intent.CATEGORY_HOME);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		}

		return super.onKeyDown(keyCode, event);
	}

    /*-----------READING FROM FIREBASE DB---------------------------------------------------------------------
     *
     * @param JSON_name
     */
    public void getJSONFromFirebase(String JSON_name) {

        final singleton_images singleton = singleton_images.getInstance();
        final String TAG = "FIREBASE";
        FirebaseDatabase mFirebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference =    mFirebaseDatabase.getReference(JSON_name);


    databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {


//                JSONArray tst = dataSnapshot.get
            JSONArray test_images = new JSONArray();
            JSONArray prod_images = new JSONArray();


            for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                String KEY = childDataSnapshot.getKey();
                Log.v(TAG,""+ childDataSnapshot.getKey()); //displays the key for the node
                Log.v(TAG,""+ childDataSnapshot.getValue());   //gives the value for given keyname
                try {

                    switch (KEY) {
                        case "ad_token":
                            JSONArray ad_details = new JSONArray(childDataSnapshot.getValue().toString());
                            singleton.setAd_details(ad_details);
                            break;
                        case "prod_images":
                            prod_images = new JSONArray(childDataSnapshot.getValue().toString());
                            break;
                        case "test_images":
                            test_images = new JSONArray(childDataSnapshot.getValue().toString());
                            break;

                        case "app_name":
                            app_name.setText(childDataSnapshot.getValue(String.class));
                            singleton.setApp_name(childDataSnapshot.getValue(String.class));
                            break;

                        case "admob_details":
                            JSONObject admob_details;
                            HashMap<String,Object> mapobj = (HashMap<String, Object>) childDataSnapshot.getValue();
                            admob_details = new JSONObject(mapobj);
                            singleton.setAdmob_details(admob_details);
                            break;

                        case "interstitial_timer":
                            singleton.setInterstitial_timer( Integer.valueOf(childDataSnapshot.getValue().toString()));
                            break;

                        case "app_mode":
                            singleton.app_mode = childDataSnapshot.getValue(String.class);
                            break;

                    }



                } catch (Exception e) {
                    e.printStackTrace();
                }
            } /*for loop ends here*/

            if (singleton.app_mode.equals("test")) {
                singleton.images_json = test_images;

            } else {
                singleton.images_json = prod_images;
            }
        }
        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    });

/*-----------READING FROM FIREBASE DB----------------------------------------------------------------------*/


}

    public void getJSONdata(){

		RequestQueue mRequestQueue;

// Instantiate the cache
		Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap

// Set up the network to use HttpURLConnection as the HTTP client.
		Network network = new BasicNetwork(new HurlStack());

// Instantiate the RequestQueue with the cache and network.
		mRequestQueue = new RequestQueue(cache, network);

// Start the queue
		mRequestQueue.start();

		String url = "https://robot01-a668c.firebaseio.com/mehandi_designs_for_hands.json";


		JsonObjectRequest jsObjRequest = new JsonObjectRequest
				(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						Log.d("JSON", response.toString());
						JSONArray app_images = new JSONArray();
						try {
							singleton_images singleton = singleton_images.getInstance();
							if (response.getString("app_mode").equals("test")) {
								app_images = response.getJSONArray("test_images");
							} else  if (response.getString("app_mode").equals("prod")){
								app_images = response.getJSONArray("prod_images");
							}


							JSONArray ad_details = response.getJSONArray("ad_token");
							JSONObject admob_details = response.getJSONObject("admob_details");
							/*Set the Ad details in Admob*/

							AdMobdetails_singleton.getInstance().setAdmob_details(admob_details);
							app_name.setText(response.getString("app_name"));
							singleton.setInterstitial_timer( response.getInt("interstitial_timer"));
							singleton.setApp_name(response.getString("app_name"));
							singleton.setAdmob_details(admob_details);
                            AdMobdetails_singleton.getInstance().app_mode = response.getString("app_mode");
                            singleton.app_mode = response.getString("app_mode");
							singleton.setImages_json(app_images);
							singleton.setAd_details(ad_details);

                            /*Dismiss Spinner*/
                            pd.dismiss();

							strip = ad.layout_strip(getApplicationContext());
							layout.addView(strip);

							ad.AdMobBanner(getApplicationContext());

							pop=new PopupWindow(getApplicationContext());
							interstitial=AdMob.get_admob_id(4,INTERSTITIAL_KEY);


						}
						catch (Exception e){
							e.printStackTrace();
						}


					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						Log.e("JSON", error.toString());
						// TODO Auto-generated method stub

					}
				});

// Add the request to the RequestQueue.
		mRequestQueue.add(jsObjRequest);
	}
	public  void networkChecking(){
		if (AppStatus.getInstance(c).isOnline()==Boolean.FALSE) {
			Log.d("No Network :",network.toString());


			Toast.makeText(getApplicationContext(),"No NetWork ",Toast.LENGTH_SHORT).show();
		}
		if (AppStatus.getInstance(c).isOnline()==Boolean.TRUE) {
			Log.d("Network Checking :",network.toString());
//			Intent i = new Intent(Activity_00.this,Pop.class);
//			startActivity(i);
			//Toast.makeText(getApplicationContext(),"No Network ",Toast.LENGTH_LONG).show();
		}

	}


}
